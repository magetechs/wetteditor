package org.wetator.editor.conversion;

import javax.inject.Inject;

import org.eclipse.xtext.Grammar;
import org.eclipse.xtext.IGrammarAccess;
import org.eclipse.xtext.conversion.IValueConverter;
import org.eclipse.xtext.conversion.IValueConverterService;
import org.eclipse.xtext.conversion.ValueConverter;
import org.eclipse.xtext.conversion.ValueConverterException;
import org.eclipse.xtext.conversion.impl.AbstractDeclarativeValueConverterService;
import org.eclipse.xtext.conversion.impl.AbstractNullSafeConverter;
import org.eclipse.xtext.nodemodel.INode;

public class WettContentValueConverters extends AbstractDeclarativeValueConverterService implements IValueConverterService {

	private Grammar grammar;

	@Inject
	public void setGrammar(IGrammarAccess grammarAccess) {
		this.grammar = grammarAccess.getGrammar();
	}

	protected Grammar getGrammar() {
		return grammar;
	}

	@ValueConverter(rule = "PARAMETER_CONTENT_LAST")
	public IValueConverter<String> PARAMETER_CONTENT_LAST() {
		return new AbstractNullSafeConverter<String>() {

			@Override
			protected String internalToString(String value) {
				return value;
			}

			@Override
			protected String internalToValue(String string, INode node) throws ValueConverterException {
				// Remove leading '|'
				int strLength = string.length() ;
				assert strLength >= 1;	// By defined definition
				return string.substring(1);
			}
		};
	}

	@ValueConverter(rule = "PARAMETER_CONTENT_FIRST")
	public IValueConverter<String> PARAMETER_CONTENT_FIRST() {
		return new AbstractNullSafeConverter<String>() {

			@Override
			protected String internalToString(String value) {
				return value;
			}

			@Override
			protected String internalToValue(String string, INode node) throws ValueConverterException {
				// Remove leading + trailing '|'
				int strLength = string.length() ;
				assert strLength >= 2;	// By defined definition
				return string.substring(1, strLength - 1);
			}
		};
	}
}
