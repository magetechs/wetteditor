/**
 * generated by Xtext 2.9.2
 */
package org.wetator.editor.wettModel.impl;

import org.eclipse.emf.ecore.EClass;

import org.wetator.editor.wettModel.ClickOn;
import org.wetator.editor.wettModel.WettModelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Click On</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ClickOnImpl extends WetatorCommandImpl implements ClickOn
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected ClickOnImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return WettModelPackage.Literals.CLICK_ON;
  }

} //ClickOnImpl
