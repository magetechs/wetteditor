/**
 * generated by Xtext 2.9.2
 */
package org.wetator.editor.wettModel.impl;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.wetator.editor.wettModel.Line;
import org.wetator.editor.wettModel.WettModelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Line</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class LineImpl extends MinimalEObjectImpl.Container implements Line
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected LineImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return WettModelPackage.Literals.LINE;
  }

} //LineImpl
