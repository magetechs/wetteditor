/**
 * generated by Xtext 2.9.2
 */
package org.wetator.editor.wettModel.impl;

import org.eclipse.emf.ecore.EClass;

import org.wetator.editor.wettModel.AssertSet;
import org.wetator.editor.wettModel.WettModelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Assert Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class AssertSetImpl extends WetatorCommandImpl implements AssertSet
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected AssertSetImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return WettModelPackage.Literals.ASSERT_SET;
  }

} //AssertSetImpl
