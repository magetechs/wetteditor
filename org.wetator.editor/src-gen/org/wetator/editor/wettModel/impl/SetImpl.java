/**
 * generated by Xtext 2.9.2
 */
package org.wetator.editor.wettModel.impl;

import org.eclipse.emf.ecore.EClass;

import org.wetator.editor.wettModel.Set;
import org.wetator.editor.wettModel.WettModelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SetImpl extends WetatorCommandImpl implements Set
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected SetImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return WettModelPackage.Literals.SET;
  }

} //SetImpl
