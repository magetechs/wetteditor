/**
 * generated by Xtext 2.9.2
 */
package org.wetator.editor.wettModel.impl;

import org.eclipse.emf.ecore.EClass;

import org.wetator.editor.wettModel.WetatorCommand;
import org.wetator.editor.wettModel.WettModelPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Wetator Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class WetatorCommandImpl extends LineImpl implements WetatorCommand
{
  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected WetatorCommandImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return WettModelPackage.Literals.WETATOR_COMMAND;
  }

} //WetatorCommandImpl
