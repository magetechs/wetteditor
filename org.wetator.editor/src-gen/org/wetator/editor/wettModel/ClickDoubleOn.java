/**
 * generated by Xtext 2.9.2
 */
package org.wetator.editor.wettModel;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Click Double On</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.wetator.editor.wettModel.WettModelPackage#getClickDoubleOn()
 * @model
 * @generated
 */
public interface ClickDoubleOn extends WetatorCommand
{
} // ClickDoubleOn
