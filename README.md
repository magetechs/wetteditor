# OPEN QUESTIONS #
* muss es ein Leerzeichen / whitespace vor und nach dem Delimiter geben? Was wenn man am Anfang des contents ein whitespace matchen m�chte. Geht das?

# TODOS #
* strg+shift+f alligned alle ersten doppelpipes auf eine h�he
* das zur Zeit aufgew�hlte keyword beim content-assist soll den Infotext (von der Webseite) rechts anzeigen
* im wetator parser nochmal schauen welche Sonderzeichen es gibt ([] > ;,) und die dann syntaktisch auch hervorheben
* noch die erweiterten command sets miteinbinden (incubator)
* Datei-Templates anlegen: mit describe || bla bla und use-module || bla bla oder open-url || bla bla
* content assist sollte sich aufs bisher ganz eingegebene keyword beziehen, nicht nur auf den letzten Buchstaben!

# DONE #
* strg+space bei einer neuen leeren Zeile soll alle keywords vorschlagen
* Wenn man dann eins ausw�hlt soll das system sofort auch doppelpipes schreiben (bis zu den anderen einger�ckt)
* Kommentare sollen farblich so sein wie in Java