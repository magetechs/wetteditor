package org.wetator.editor.ui;

import org.eclipse.jface.text.contentassist.ContentAssistant;
import org.eclipse.jface.text.contentassist.IContentAssistant;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.jface.text.source.SourceViewerConfiguration;
import org.eclipse.xtext.ui.editor.contentassist.DefaultContentAssistantFactory;
import org.eclipse.xtext.ui.editor.contentassist.IContentAssistantFactory;

public class MyContentAssistFactory extends DefaultContentAssistantFactory {

	public Class<? extends IContentAssistantFactory> bindIContentAssistantFactory() {
		return MyContentAssistFactory.class;
	}
	
	@Override
	protected void setContentAssistProcessor(ContentAssistant assistant, SourceViewerConfiguration configuration,
			ISourceViewer sourceViewer) {
		super.setContentAssistProcessor(assistant, new MySourceViewerConfiguration(), sourceViewer);
	}	
	
	@Override
	protected void configureContentAssistant(ContentAssistant assistant, SourceViewerConfiguration configuration,
			ISourceViewer sourceViewer) {
		super.configureContentAssistant(assistant,  new MySourceViewerConfiguration(), sourceViewer);
	}
	
	
	@Override
	public IContentAssistant createConfiguredAssistant(SourceViewerConfiguration configuration,
			ISourceViewer sourceViewer) {
		return super.createConfiguredAssistant( new MySourceViewerConfiguration(), sourceViewer);
	}
	
	


}
