package org.wetator.editor.ui;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter;
import org.eclipse.xtext.formatting.impl.FormattingConfig;
import org.eclipse.xtext.parsetree.reconstr.ITokenStream;
import org.wetator.editor.services.WettModelGrammarAccess;

public class WettFormatter extends AbstractDeclarativeFormatter{

	@Override
	protected void configureFormatting(FormattingConfig config) {
		WettModelGrammarAccess grammar = (WettModelGrammarAccess)getGrammarAccess();

//        config.setSpace("   ").after(grammar.getSetRule());
//        config.setSpace("tee").after(grammar.getAssertContentRule());
        config.setLinewrap(3).before(grammar.getSetRule());
        config.setNoSpace();
        config.setAutoLinewrap(3);
        config.setIndentationDecrement().before(grammar.getLineRule());
        config.setWrappedLineIndentation(2);
        config.setLinewrap().before(grammar.getAssertTitleRule());
        config.setLinewrap().before(grammar.getLineRule());
        config.setLinewrap().before(grammar.getModelRule());
        config.setLinewrap().before(grammar.getPARAMETER_CONTENT_FIRSTRule());
        config.setLinewrap().before(grammar.getPARAMETER_CHARRule());
        config.setLinewrap().before(grammar.getParameterList1Rule());
        config.setSpace("\t").after(grammar.getPARAMETER_CONTENT_FIRSTRule());
        
        System.out.println("formatted!");
	}
	
	@Override
	public ITokenStream createFormatterStream(EObject context, String indent, ITokenStream out,
			boolean preserveWhitespaces) {
		// TODO Auto-generated method stub
		return super.createFormatterStream(context, indent, out, preserveWhitespaces);
	}

}
