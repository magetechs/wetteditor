/*
 * generated by Xtext 2.9.1
 */
package org.wetator.editor.ui.contentassist

import com.google.inject.Inject
import java.util.Map
import org.eclipse.emf.ecore.EObject
import org.eclipse.jface.viewers.StyledString
import org.eclipse.swt.graphics.Image
import org.eclipse.xtext.AbstractElement
import org.eclipse.xtext.Keyword
import org.eclipse.xtext.RuleCall
import org.eclipse.xtext.ui.editor.contentassist.ConfigurableCompletionProposal
import org.eclipse.xtext.ui.editor.contentassist.ContentAssistContext
import org.eclipse.xtext.ui.editor.contentassist.ICompletionProposalAcceptor
import org.wetator.editor.services.WettModelGrammarAccess

/**
 * See https://www.eclipse.org/Xtext/documentation/304_ide_concepts.html#content-assist
 * on how to customize the content assistant.
 */
class WettModelProposalProvider extends AbstractWettModelProposalProvider {

	val KEYWORD_SPACES = 16;
	val DELIMITER = "||"

	@Inject
	WettModelGrammarAccess grammarAccess;

	final static public Map<String, String> KEYWORD_DESCRIPTIONS = newHashMap()

	var initialized = false;

	def init() {
		KEYWORD_DESCRIPTIONS.put(grammarAccess.assertContentAccess.assertContentKeyword_0.value, "TEST")
		KEYWORD_DESCRIPTIONS.put(grammarAccess.assertDeselectedAccess.assertDeselectedKeyword_0.value, '''
			<p>Asserts, that an entry from a list is deselected.</p>
			<div>
				Use this to check the selection from a single or multiple select list, a group of radio buttons or select a checkbox.
			</div>
			<h2>Parameters</h2>
			<br />
			<dl>
				<dt>wpath</dt>
				<dd>
					The WPath identifying the item to check (see WPath for a detailed description).
					<dl>
						<dt>Single Select Box</dt>
						<dd>
						   	The path is usually the label of the select box, a > and then the text of the option to check.
						  	</dd>
						<dt>Multiple Select Box</dt>
						<dd>
						   	To make more than one selection in a multiple select box, use more than one Assert Select call.
						   </dd>
						<dt>Radio Group</dt>
						   <dd>Use the label of the radio button you like to select. If this text is not unique you can as always define some words before to make the path unique.</dd>
						<dt>Check Box</dt>
						   <dd>Use the text usually placed after the checkbox you like to check. If this text is not unique you can as always define some words before to make the path unique.</dd>
					</dl>
				</dd>
			</dl>
			<div>
					It is possible to use the '*' and the '?' character als wildcards; '*' matches zero or many characters; '?' matches one single character.
					It is also possible to define a > separated list of words (with '*' and '?' if needed). Then this command checks the first element after all these words.
			</div>
		''')
		KEYWORD_DESCRIPTIONS.put(grammarAccess.assertDisabledAccess.assertDisabledKeyword_0.value, null)
		KEYWORD_DESCRIPTIONS.put(grammarAccess.assertEnabledAccess.assertEnabledKeyword_0.value, null)
		KEYWORD_DESCRIPTIONS.put(grammarAccess.assertSelectedAccess.assertSelectedKeyword_0.value, null)
		KEYWORD_DESCRIPTIONS.put(grammarAccess.assertSetAccess.assertSetKeyword_0.value, null)
		KEYWORD_DESCRIPTIONS.put(grammarAccess.assertTitleAccess.assertTitleKeyword_0.value, null)
		KEYWORD_DESCRIPTIONS.put(grammarAccess.clickDoubleOnAccess.clickDoubleOnKeyword_0.value, null)
		KEYWORD_DESCRIPTIONS.put(grammarAccess.clickOnAccess.clickOnKeyword_0.value, null)
		KEYWORD_DESCRIPTIONS.put(grammarAccess.clickRightOnAccess.clickRightOnKeyword_0.value, null)
		KEYWORD_DESCRIPTIONS.put(grammarAccess.closeWindowAccess.closeWindowKeyword_0.value, null)

	}

	override complete_Line(EObject model, RuleCall ruleCall, ContentAssistContext context,
		ICompletionProposalAcceptor acceptor) {
		super.complete_Line(model, ruleCall, context, acceptor)
		if (!initialized) {
			init()
			initialized = true
		}
		for (AbstractElement element : context.getFirstSetGrammarElements()) {
			if (element instanceof Keyword) {
				val keyword = element as Keyword
				val value = keyword.value
				var keywordDescription = KEYWORD_DESCRIPTIONS.get(value)
				// e.g. use-module                ||
				val proposal = value + computeSpaces(value) + DELIMITER
				var styledValue = new StyledString(value, [textStyle|textStyle.underline = true])
				if (keywordDescription == null) {
					keywordDescription = "NO DESCRIPTION YET"
				} 
				acceptor.accept(
					createCompletionProposal(proposal, styledValue, null, 700, context.prefix, context,
						keywordDescription));
			}
		}
	}

	def String computeSpaces(String keywordValue) {
		// Number of spaces the keyword itself already brings
		val keywordSpaces = keywordValue.length;
		val numberOfSpacesNeeded = KEYWORD_SPACES - keywordSpaces
		var spaces = "";
		for (var i = 1; i <= numberOfSpacesNeeded; i++) {
			spaces += " "
		}
		spaces
	}

	def protected createCompletionProposal(String proposal, StyledString displayString, Image image, int priority,
		String prefix, ContentAssistContext context, String additionalInformation) {
		if (isValidProposal(proposal, prefix, context)) {
			return doCreateProposal(proposal, displayString, image, priority, context, additionalInformation);
		}
		return null;
	}

	def protected doCreateProposal(String proposal, StyledString displayString, Image image, int priority,
		ContentAssistContext context, String additionalInformation) {
		var replacementOffset = context.getReplaceRegion().getOffset();
		var replacementLength = context.getReplaceRegion().getLength();
		var result = new ConfigurableCompletionProposal(proposal, replacementOffset, replacementLength,
			proposal.length(), image, displayString, null, additionalInformation);

		result.setPriority(priority);
		result.setMatcher(context.getMatcher());
		result.setReplaceContextLength(context.getReplaceContextLength());
		return result;
	}
}
